
Kafka setup 

A- Downloading the Required Files
Download Server JRE according to your OS and CPU architecture from http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html
Download and install 7-zip from http://www.7-zip.org/download.html
Download and extract Zookeeper using 7-zip from http://zookeeper.apache.org/releases.html
Download and extract Kafka using 7-zip from http://kafka.apache.org/downloads.html

A.1- Set up java 

B. Zookeeper Installation
1. Go to your Zookeeper config directory. For me its C:\zookeeper-3.4.7\conf
2. Rename file “zoo_sample.cfg” to “zoo.cfg”
3. Open zoo.cfg in any text editor, like Notepad; I prefer Notepad++.
4. Find and edit dataDir=/tmp/zookeeper to :\zookeeper-3.4.7\data  
5. Add an entry in the System Environment Variables as we did for Java.
a. Add ZOOKEEPER_HOME = C:\zookeeper-3.4.7 to the System Variables.
b. Edit the System Variable named “Path” and add ;%ZOOKEEPER_HOME%\bin; 
6. You can change the default Zookeeper port in zoo.cfg file (Default port 2181).
7. Run Zookeeper by opening a new cmd and type zkserver.

C. Setting Up Kafka
1. Go to your Kafka config directory. For me its C:\kafka_2.11-0.9.0.0\config
2. Edit the file “server.properties.”
3. Find and edit the line log.dirs=/tmp/kafka-logs” to “log.dir= C:\kafka_2.11-0.9.0.0\kafka-logs.
4. If your Zookeeper is running on some other machine or cluster you can edit “zookeeper.connect:2181” to your custom IP and port.
 For this demo, we are using the same machine so there's no need to change. Also the Kafka port and broker.id are configurable in this file. Leave other settings as is.
5. Your Kafka will run on default port 9092 and connect to Zookeeper’s default port, 2181.

D. Running a Kafka Server
Important: Please ensure that your Zookeeper instance is up and running before starting a Kafka server.
1. Go to your Kafka installation directory: C:\kafka_2.11-0.9.0.0\
2. Open a command prompt here by pressing Shift + right click and choose the “Open command window here” option).
3. Now type .\bin\windows\kafka-server-start.bat .\config\server.properties and press Enter.


Kafka Connect
1- Start kafka server
   .\bin\windows\kafka-server-start.bat .\config\server.properties
2- Start kafka topic
   .\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
3- Create kafka producer
   .\bin\windows\kafka-console-producer.bat --broker-list localhost:9092 --topic test
4- Create kafka consumer
   .\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic test --from-beginning

5- Delete Topic
    5.1 Mark topic as delete
       .\bin\windows\kafka-topics.bat --zookeeper localhost:2181 --delete --topic test
	5.2 open zkCli
	5.3 get /brokers/topics/<topic_name>
	5.4 rmr /brokers/topics/<topic_name>
    5.5 rmr /admin/delete_topics/<topic_name>



